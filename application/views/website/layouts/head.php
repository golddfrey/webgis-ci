<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Safario Travel - Home</title>
	<link rel="icon" href="img/Fevicon.png" type="image/png">

  <link rel="stylesheet" href="<?=templates('vendors/bootstrap/bootstrap.min.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/fontawesome/css/all.min.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/themify-icons/themify-icons.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/linericon/style.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/owl-carousel/owl.theme.default.min.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/owl-carousel/owl.carousel.min.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/flat-icon/font/flaticon.css','website')?>">
  <link rel="stylesheet" href="<?=templates('vendors/nice-select/nice-select.css','website')?>">

  <link rel="stylesheet" href="<?=templates('css/style.css','website')?>">
</head>